
<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    TCEFORM.tt_content.imageorient.types.image.keepItems = 0,1,2

    TCEFORM.tt_content.image_noRows.types.textpic.disabled = 1
    TCEFORM.tt_content.imageorient.types.textpic.keepItems = 17,18,25,26
    TCEFORM.tt_content.imagewidth.types.textpic.keepItems = 1,2,3

	TCEFORM.tt_content.image.types.textpic.config.minitems = 1
    '
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup','
    # add JS for Click On Large function
    page.includeJSFooter.ImageZoom = typo3conf/ext/hn_contentelements/Resources/Public/JavaScript/ImageZoom.js

    # Ctype = image
    tt_content.image >
	tt_content.image = COA
	tt_content.image {
	    10 = < lib.stdheader

        20 = FLUIDTEMPLATE
        20 {
            template = FILE
            template.file = EXT:hn_contentelements/Resources/Private/Templates/Image.html
            variables {
                images = FILES
                images {
                    references{
                        table = tt_content
                        fieldName = image
                    }

                    renderObj = TEXT
                    renderObj {
                        data = file:current:uid
                        stdWrap.wrap = ,
                    }
                }
            }
        }
	}

    # Ctype = textpic
    tt_content.textpic >
	tt_content.textpic < tt_content.image
	tt_content.textpic.20.template.file = EXT:hn_contentelements/Resources/Private/Templates/Textpic.html

', 43
);

?>
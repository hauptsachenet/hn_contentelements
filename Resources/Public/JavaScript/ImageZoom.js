(function ($) {
    "use strict";

    var $document = $(document);
    var $window = $(window);

    var windowMinSize = Math.min(screen.width, screen.height);
    var useFullscreen = document.fullscreenEnabled !== undefined && windowMinSize < 1024;
    console.log('use fullscreen', useFullscreen, windowMinSize);

    var enableFullscreen = function (element) {
        toogleImage(element);
        $(element).addClass('fullscreen-element');

        if (useFullscreen) {
            element.requestFullscreen();
        } else {
            $('<div class="fullscreen-backdrop"></div>').appendTo('body');
        }
    };

    var disableFullscreen = function (element) {
        toogleImage(element);
        $(element).removeClass('fullscreen-element');

        if (useFullscreen) {
            document.exitFullscreen();
        } else {
            $('.fullscreen-backdrop').remove();
        }
    };

    var toogleImage = function(imageParent) {
        var imageElm = $(imageParent).find('img[data-fullscreen-toogle]').first();
        var imageSrc = imageElm.attr('src');
        imageElm.attr('src', imageElm.data('fullscreen-toogle'));
        imageElm.data('fullscreen-toogle', imageSrc);
    };

    var isFullscreenEnabled = function () {
        if (useFullscreen) {
            return document.fullscreenEnabled;
        } else {
            return $('.fullscreen-backdrop').length > 0;
        }
    };

    $document.on('click', '*:has(>img[data-fullscreen-toogle])', function (e) {
        e.preventDefault();

        if (isFullscreenEnabled()) {
            disableFullscreen(this);
        } else {
            enableFullscreen(this);
        }
    });

    var disableGlobalFullscreen = function () {
        disableFullscreen($('.fullscreen-element')[0]);
    };

    $document.on('click', '.fullscreen-backdrop', disableGlobalFullscreen);

    $document.on('keyup', function (e) {
        if (e.keyCode == 27) {
            disableGlobalFullscreen();
        }
    });

})(jQuery);
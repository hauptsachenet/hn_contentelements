<?php


$EM_CONF[$_EXTKEY] = array (
	'title' => 'Custom content elements',
	'description' => 'Modify image and textpic typo3 elements to use bootstrap responsive layout',
	'category' => 'plugin',
	'shy' => 0,
	'version' => '2.0.0',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => 'mod1',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'lockType' => '',
	'author' => '',
	'author_email' => '',
	'author_company' => '',
	'CGLcompliance' => NULL,
	'CGLcompliance_note' => NULL,
);

?>
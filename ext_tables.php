<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// change imagewidth field to select
$GLOBALS['TCA']['tt_content']['columns']['imagewidth'] = array(
    'label' => 'Width',
    'config' => array(
        'type' => 'select',
        'renderType' => 'selectSingle',
        'items' => array(
            array('full (1/1)', '0'),
            array('half (1/2)', '1'),
            array('third (1/3)', '2'),
            array('quarter (1/4)', '3'),
        ),
    )
);

# modify imageblock palette
$GLOBALS['TCA']['tt_content']['palettes']['imageblock'] = array(
    'showitem' => 'imageorient;LLL:EXT:cms/locallang_ttc.xlf:imageorient_formlabel,imagewidth,image_noRows;LLL:EXT:cms/locallang_ttc.xlf:image_noRows_formlabel,imageborder;LLL:EXT:cms/locallang_ttc.xlf:imageborder_formlabel',
    'canNotCollapse' => 1
);
unset($GLOBALS['TCA']['tt_content']['palettes']['image_settings']);


//exchange imageorient "above center" & "above left", but keep value
$newImageorientAboveCenter = $GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][0];
$newImageorientAboveCenter[1] = $GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][2][1];
$newImageorientAboveLeft = $GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][2];
$newImageorientAboveLeft[1] = $GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][0][1];

$GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][0] = $newImageorientAboveLeft;
$GLOBALS['TCA']['tt_content']['columns']['imageorient']['config']['items'][2] = $newImageorientAboveCenter;

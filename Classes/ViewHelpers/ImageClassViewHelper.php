<?php
namespace Hn\HnContentelements\ViewHelpers;


class ImageClassViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param array $data
	 * @return string
	 */
	public function render($data) {

		$class = array();
		$class[] = $this->getImageWidthClass($data['imagewidth']);
		$class[] = $this->getImageOrientClass($data['imageorient']);

		if($data['imageborder']) {
			$class[] = 'img-bordered';
		}

		if ($data['CType'] == 'image' && !$data['image_noRows']) {
			$class[] = 'view-rows';
		}

		return implode(' ', $class);
	}

	/**
	 * @param $imagewidth
	 * @return string
	 */
	protected function getImageWidthClass($imagewidth) {
		switch($imagewidth) {
			case 0:
				return 'imagewidth-full';
			case 1:
				return 'imagewidth-half';
			case 2:
				return 'imagewidth-third';
			case 3:
				return 'imagewidth-quarter';
		}
	}

	/**
	 * @param $imageorient
	 * @return string
	 */
	protected function getImageOrientClass($imageorient) {

		switch ($imageorient) {
			case 0:
				return 'align-left';
			case 1:
				return 'align-right';
			case 2:
				return 'align-center';
			case 17:
				return 'in-text-right';
			case 18:
				return 'in-text-left';
			case 25:
				return 'beside-text-right';
			case 26:
				return 'beside-text-left';
		}
	}
}

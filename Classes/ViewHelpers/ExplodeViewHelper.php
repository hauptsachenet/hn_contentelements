<?php
namespace Hn\HnContentelements\ViewHelpers;


use TYPO3\CMS\Core\Utility\GeneralUtility;

class ExplodeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    protected $escapeOutput = false;

	/**
	 * @param string $data
	 * @param string $as
	 * @param string $delimiter
	 * @return string
	 */
	public function render($data, $as = 'items', $delimiter = ',') {

		if ($data) {
			$items = GeneralUtility::trimExplode($delimiter, $data, TRUE);
			$this->templateVariableContainer->add($as, $items);
			$content = $this->renderChildren();
			$this->templateVariableContainer->remove($as);
		}
		return $content;
	}

}

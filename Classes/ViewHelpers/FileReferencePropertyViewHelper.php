<?php
namespace Hn\HnContentelements\ViewHelpers;

class FileReferencePropertyViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


	/**
	 * @param $fileReferenceUid int
	 * @param $property string
	 * @return array
	 */
	public function render($fileReferenceUid, $property) {
		$fileReferenceObject = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance()->getFileReferenceObject($fileReferenceUid);

		$methodName = 'get'. ucfirst($property);
		if(method_exists($fileReferenceObject, $methodName)) {
			return $fileReferenceObject->{$methodName}();
		}

		$fileReferenceProperties =  $fileReferenceObject->getProperties();
		if(!array_key_exists($property, $fileReferenceProperties)) {
			throw new \Exception('property:'.$property.' in class:'.get_class($fileReferenceObject).' is not exists. ');
		}

		return $fileReferenceProperties[$property];
	}


}

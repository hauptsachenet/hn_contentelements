<?php
namespace Hn\HnContentelements\ViewHelpers;


use TYPO3\CMS\Core\Utility\GeneralUtility;

class ImageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper  implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $cObj;

    protected $escapeOutput = false;

    /**
     * construct
     */
    public function __construct() {
        parent::__construct();
        $this->cObj = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
    }

    /**
     * Initialize arguments.
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('typolinkParams', 'string', '', false, '');
        $this->registerArgument('imageZoom', 'bool', '', false, false);
        $this->registerTagAttribute('data-fullscreen-toogle', 'string', 'Image for fullscreen', false);
        parent::initializeArguments();
    }

    /**
     * Resizes a given image (if required) and renders the respective img tag
     *
     * @see https://docs.typo3.org/typo3cms/TyposcriptReference/ContentObjects/Image/
     * @param FileInterface|AbstractFileFolder $image a FAL object
     * @param bool $imageZoom
     * @param string $typolinkParams typolink params
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     * @return string Rendered tag
     */
    public function render()
    {
        $src = $this->arguments['src'];
        $treatIdAsReference = $this->arguments['treatIdAsReference'];
        $image = $this->arguments['image'];
        $crop = $this->arguments['crop'];
        $typolinkParams = $this->arguments['typolinkParams'];
        $absolute = $this->arguments['absolute'];
        $imageZoom = $this->arguments['imageZoom'];

        // create zoom image if not set link for this image
        if($imageZoom && !$typolinkParams) {
            $fullScreenImageUri = \TYPO3\CMS\Fluid\ViewHelpers\Uri\ImageViewHelper::renderStatic (
                array(
                    'src' => $src,
                    'image' => $image,
                    'width' => null,
                    'height' => null,
                    'minWidth' => null,
                    'minHeight' => null,
                    'maxWidth' => 1200,
                    'maxHeight' => 1200,
                    'treatIdAsReference' => $treatIdAsReference,
                    'crop' => $crop,
                    'absolute' => $absolute,
                ),
                $this->buildRenderChildrenClosure(),
                $this->renderingContext
            );

            $this->tag->addAttribute('data-fullscreen-toogle', $fullScreenImageUri);
        }

        $image = parent::render();

        // create link if set
        if($typolinkParams) {
            return $this->cObj->getTypoLink($image, $typolinkParams);
        }

        return $image;
    }
}
